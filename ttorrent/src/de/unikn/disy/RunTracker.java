/** Copyright (C) 2011 Thomas Zink, Uni Konstanz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.unikn.disy;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import com.turn.ttorrent.common.Torrent;
import com.turn.ttorrent.tracker.Tracker;

/**
 * @author zink
 */
public final class RunTracker {

	private static String version = "0.1";
	private static String DEFAULT_HOST = "localhost";
	
	private static void usage() {
		System.out.println("Runs a tracker on provided address and port.\n");
		System.out.println("Usage: " +
				"tracker <-f filename> [-i ip] [-p port]");
		System.out.println("\tfilename\t-\tTorrent-file the tracker will track.");
		System.out.println("\tip\t\t-\tIP address or hostname to bind to. " +
				"Default: 'localhost'");
		System.out.println("\tport\t-\tPort to bind to. Default: " +
				"tracker's default port " + Tracker.DEFAULT_TRACKER_PORT);
	}
	
	/**
	 * Runs a tracker.
	 * @param args
	 */
	public static void main(String[] args) {
		String addrstr = DEFAULT_HOST;
		int port = Tracker.DEFAULT_TRACKER_PORT;
		InetAddress addr;
		File source = null;
		
		Tracker tracker;
		Torrent torrent;
		
		int i=0;
		String arg = "";
		
		while (i < args.length && args[i].startsWith("-")) {
            arg = args[i++];
            if (arg.equals("--help") || arg.equals("-h")) {
                usage(); return;
            } else if (arg.equals("-i")) {
            	if (i < args.length) addrstr = args[i++];
            	else {
            		usage(); return;
            	}
            } else if (arg.equals("-p")) {
            	if (i < args.length) {
            		try { port = Integer.parseInt(args[i++]); }
            		catch (NumberFormatException e) { usage(); return; }
            	} else {
            		usage(); return;
            	}
            } else if (arg.equals("-f")) {
            	if (i < args.length) {
            		source = new File(args[i++]);
            	} else {
            		usage(); return;
            	}
            }
		}
		
		if (source == null || !source.exists()) {
			usage(); return;
		}
		
		try {
			if (addrstr.equals(DEFAULT_HOST)) {
				addr = InetAddress.getLocalHost();
			} else {
				addr = InetAddress.getByName(addrstr);
			}
		} catch (UnknownHostException e) { 
			e.printStackTrace();
			return;
		}
		
		try {
			tracker = new Tracker(addr, version, port);
		} catch (IOException e) {
			usage(); return;
		}
		
		byte[] torrentBytes = new byte[(int)source.length()];
		FileInputStream fis;
		try {
			fis = new FileInputStream(source);
			fis.read(torrentBytes);
			fis.close();
		} catch (FileNotFoundException e) {
			usage(); return;
		} catch (IOException e) {
			usage(); return;
		}
		
		torrent = new Torrent(torrentBytes);
		
		/*try {
			torrent = Torrent.create(source, tracker.getAnnounceUrl(), "anonymous");
		} catch (NoSuchAlgorithmException e) {
			usage(); return;
		} catch (IOException e) {
			usage(); return;
		}*/
		
		
		tracker.announce(torrent);
		tracker.start();
	}

}
