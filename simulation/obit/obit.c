#include "sha1.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <math.h>
#include <stdbool.h>
#include <time.h>

uint16_t niterations = 0;

typedef struct bignumber_s {
		uint8_t data[20];
} peer_id, info_hash;

typedef struct handshake_s {
		uint8_t data[20 + 8 + 20 + 20];
} handshake;

bool is_magic_peer_id(const peer_id *pid) {
		SHA1Context sha;
		SHA1Reset(&sha);
		SHA1Input(&sha, (const unsigned char *) pid, 20);
		if (SHA1Result(&sha)) {
				return !(sha.Message_Digest[4] & 0x0000FFFF);
		}
		return false;
}

void random_peer_id (const peer_id* pid) {
		int i = 0;
		uint8_t *p = (uint8_t*) pid;
		for (;i < 20; i++, p++) *p = rand() % 0xFF;
}

peer_id* generate_magic_peer_id () {
		bool not_found = true;
		peer_id *pid = (peer_id*) malloc(sizeof(peer_id)) ;
		while (not_found) {
				niterations++;
				random_peer_id(pid);
				not_found = !is_magic_peer_id(pid);
		}
		return pid;
}

uint8_t * padding () {
	uint32_t length = 1 + rand() % 1400 ;
	uint8_t type = 0xa;
	uint8_t *msg = (uint8_t*) malloc(length+4);
	uint32_t i = 0;

	memcpy(msg, &length, 4); 
	memcpy(msg+4, &type, 1);
	uint8_t *p = msg + 5;
	for (i=0; i<length-1; i++, p++) *p = rand() % 0xff;
	return msg;
}

void obfuscate_handshake (const handshake* hs, const peer_id* remote) {
		uint8_t *pidih = (uint8_t*) malloc(sizeof(peer_id) + sizeof(info_hash));
		uint8_t *p = pidih;
		memcpy(p, ((uint8_t*) hs)+28, 20); p += 20;
		memcpy(p, remote, 20);
		SHA1Context sha;
		SHA1Reset(&sha);
		SHA1Input(&sha, (const unsigned char *) pidih, 40);
		if (SHA1Result(&sha)) {
				memcpy((uint8_t *)hs, (uint8_t*) sha.Message_Digest, 20);
		}
		p = pidih;
		memcpy(p, remote, 20); p += 20;
		memcpy(p, ((uint8_t*) hs)+28, 20);
		SHA1Reset(&sha);
		SHA1Input(&sha, (const unsigned char *) pidih, 40);
		p = (uint8_t*) hs; p += 20;
		if (SHA1Result(&sha)) {
				uint64_t *popt = (uint64_t*)(((uint8_t*)(hs))+20);
				uint64_t orsvd = *popt ^ (*(uint64_t*)(sha.Message_Digest));
				memcpy(p, &orsvd, 8);
		}
}

handshake * write_handshake (const peer_id *local, const peer_id *remote, const info_hash *ih) {
		handshake *hs = (handshake*) malloc(20+8+20+20);
		uint8_t *p = (uint8_t*) hs;
		memcpy(p, "\x13", 1); p++;
		memcpy(p,"BitTorrent protocol", 0x13); p += 0x13;
		memcpy(p, "\x0000000000000000", 8); p += 8;
		memcpy(p, ih, 20); p += 20;
		memcpy(p, local, 20);
		if (is_magic_peer_id(local) && is_magic_peer_id(remote)) {
				obfuscate_handshake(hs, remote);
		}
		return hs;
}

bool read_handshake (const handshake *hs, const peer_id *local) {
		uint8_t *pidih = (uint8_t*) malloc(sizeof(peer_id) + sizeof(info_hash));
		uint8_t *p = pidih;
		memcpy(p, ((uint8_t*) hs) + 28, 20); p += 20;
		memcpy(p, local, 20);
		SHA1Context sha;
		SHA1Reset(&sha);
		SHA1Input(&sha, (const unsigned char *) pidih, 40);
		if (SHA1Result(&sha)) {
				int n = memcmp(hs, (uint8_t*) sha.Message_Digest, 20);
				if (n == 0) {
						p = pidih;
						memcpy(p, local, 20); p+=20;
						memcpy(p, ((uint8_t*)hs) + 28, 20);
				
						SHA1Reset(&sha);
						SHA1Input(&sha, (const unsigned char*) pidih, 40);
						uint64_t rsvd;
						if (SHA1Result(&sha)) {
								uint64_t *popt = (uint64_t*)(((uint8_t*)(hs))+20);
								rsvd = *popt ^ (*(uint64_t*)(sha.Message_Digest));
								memcpy((uint8_t*) (((uint8_t*) hs) + 20), &rsvd, 8);
						}
				}
				return true;
		}
		return false;
}

int test_magic (int runs) {
	int i = runs;
	int totaltries = 0;
	while (i--) {
		peer_id * pid = generate_magic_peer_id();
		totaltries += niterations;
		printf("%d,", niterations);
		niterations = 0;
		free(pid);
	}
	return totaltries;
}

int main (int argc, char** argv) {
		int i = 0;
		srand(time(NULL));
		
		if (argc>1) {
			int runs = atoi(argv[1]);
			time_t start, stop;
			time(&start);
			int totaltries = test_magic(runs);
			time(&stop);
			double duration = difftime(stop,start);
			printf("\n%d, %d, %f, %d\n", runs, totaltries, (float)totaltries/(float)runs, (int)(duration*1000));
			return 0;
		}
		
		peer_id *pid = generate_magic_peer_id();
		uint8_t *p = (uint8_t*) pid;
		printf("found local peer ID after %u iterations.\n", niterations);
		niterations = 0;
		for (;i < 20; i++, p++) printf("%02x ", *p);
		printf("\n\n");
		
		peer_id *remote = generate_magic_peer_id();
		printf("found remote peer ID after %u iterations.\n", niterations);
		p = (uint8_t*) remote;
		for (i = 0; i < 20; i++, p++) printf("%02x ", *p);
		printf("\n\n");
		
		info_hash *ih = (info_hash*) malloc(sizeof(info_hash));
		random_peer_id(ih);
		printf("info hash.\n");
		p = (uint8_t*) ih;
		for (i = 0; i < 20; i++, p++) printf("%02x ", *p);
		printf("\n\n");
		
		handshake *hs = write_handshake(pid, remote, ih);
		printf("handshake created.\n");
		p = (uint8_t*) hs;
		for (i = 0; i < 68; i++, p++) {
				if (i == 20 || i == 28 || i == 48) printf("\n");
				printf("%02x ", *p);
		}
		printf("\n\n");
		
		uint8_t *pad = padding();
		uint32_t *length = (uint32_t*) pad;
		printf("padding created, length: %d, type %d.\n", *length, *(pad+4));
		p = pad;
		for (i=0; i<*length; i++, p++) printf("%02x ", *p);
		printf("\n\n");
		
		bool is_obfuscated = read_handshake(hs, remote);
		if (is_obfuscated) printf ("received obfuscated handshake.\n");
		printf("handshake decoded.\n");
		p = (uint8_t*) hs;
		for (i = 0; i < 68; i++, p++) {
				if (i == 20 || i == 28 || i == 48) printf("\n");
				printf("%02x ", *p);
		}
		printf("\n");
		
		return 0;
}
